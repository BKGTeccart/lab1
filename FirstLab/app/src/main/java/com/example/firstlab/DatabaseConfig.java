package com.example.firstlab;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseConfig extends SQLiteOpenHelper {

    SQLiteDatabase database;
    public static final String DATABASE_NAME = "gestion_client.db";

    public DatabaseConfig(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
        database = getWritableDatabase();
    }

    //Client
    public static final String TABLE_1 = "Client";


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // create database
        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_1+" (Id_client INTEGER PRIMARY KEY AUTOINCREMENT, Nom_client TEXT, Prenom_client TEXT, Adresse_client TEXT, Username TEXT, Password TEXT, Solde DOUBLE, Credit DOUBLE)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // alter data base
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_1);
        onCreate(sqLiteDatabase);

    }

    //fonctions insert | update | delete | select | CRUD

    //Insertion client
    public boolean insert_client(String nom, String prenom, String adresse, String username, String password, Double solde, Double credit){
        SQLiteDatabase db_ = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //contentValues.put(nom de la colonne, la valeur)
        contentValues.put("Nom_client", nom);
        contentValues.put("Prenom_client", prenom);
        contentValues.put("Adresse_client", adresse);
        contentValues.put("Username", username);
        contentValues.put("Password", password);
        contentValues.put("Solde", solde);
        contentValues.put("Credit", credit);

        long result = db_.insert(TABLE_1, null, contentValues);

        if(result!=-1){
            return true;
        } else {
            return false;
        }

    }

    //Select : Liste des comptes delinquants
    public Cursor getListAccounts(){
        SQLiteDatabase db_ = this.getReadableDatabase();

        Cursor results = db_.rawQuery("SELECT * FROM "+TABLE_1+" WHERE Solde>Credit", null);
        return results;

    }

    //Select : Liste des comptes
    public Cursor getListAccounts_(){
        SQLiteDatabase db_ = this.getReadableDatabase();

        Cursor results = db_.rawQuery("SELECT * FROM "+TABLE_1, null);
        return results;
    }

    //Select : Liste des comptes
    public Cursor getInfosAccount(int id_client){
        SQLiteDatabase db_ = this.getReadableDatabase();

        Cursor results = db_.rawQuery("SELECT * FROM "+TABLE_1+" WHERE Id_client="+id_client, null);
        return results;
    }

    //Update : modifier compte
    public void updateConfig(int id_client, String nom, String prenom, String adresse, String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_1+" SET Nom_client='"+nom+"', Prenom_client='"+prenom+"', Adresse_client='"+adresse+"', Username='"+username+"', Password='"+password+"' WHERE Id_client="+id_client);
    }

    //Update : modifier credit
    public void updatecredit(int id_client, Double credit){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_1+" SET Credit="+credit+" WHERE Id_client="+id_client);
    }
    public void updatesolde(int id_client, Double solde){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_1+" SET Solde="+solde+" WHERE Id_client="+id_client);
    }

    //Delete : delete account
    public void deleteAccount(int id_client){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_1+" WHERE Id_client="+id_client);
    }




}
