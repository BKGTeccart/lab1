package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class ModifierCreditActivity extends AppCompatActivity {

    Spinner sp_clients_credit;
    DatabaseConfig db_conf;
    ArrayList<String> list_clients = new ArrayList<>();
    EditText edt_credit;
    int current_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_credit_acctivity);

        sp_clients_credit = findViewById(R.id.sp_clientscredit);
        edt_credit = findViewById(R.id.edt_credit_client);
        db_conf = new DatabaseConfig(this);
        Cursor res_ = db_conf.getListAccounts_();

        if(res_.getCount()>0){

            while(res_.moveToNext()){
                // colonne 1 : nom_client | colonne 2: prenom_client
                list_clients.add(String.valueOf(res_.getInt(0)));
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, list_clients);
            sp_clients_credit.setAdapter(arrayAdapter);

            //Quand tu cliques sur un item du dropdown
            sp_clients_credit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(ModifierCompteActivity.this, list_clients.get(position), Toast.LENGTH_LONG).show();
                    Cursor res_2 = db_conf.getInfosAccount(Integer.parseInt(list_clients.get(position)));
                    current_id = Integer.parseInt(list_clients.get(position));

                    while(res_2.moveToNext()){
                        // infos client
                        edt_credit.setText(String.valueOf(res_2.getDouble(7)));
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

        }else {
            Toast.makeText(this, "Aucun enregistrement trouvé!", Toast.LENGTH_LONG).show();
        }
    }
    public void btn_retour(View view){
        //finir tous les processus de l'activité actuelle
        finish();
    }
    public void btn_modifier_credit(View view){
        // parse to double : Double.parseDouble(String)
        db_conf.updatecredit(current_id, Double.parseDouble(edt_credit.getText().toString()));
        Toast.makeText(this, "Modifié!", Toast.LENGTH_LONG).show();
        finish();
    }
}