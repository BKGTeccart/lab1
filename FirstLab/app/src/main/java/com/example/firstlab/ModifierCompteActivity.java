package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class ModifierCompteActivity extends AppCompatActivity {

    Spinner sp_clients;
    DatabaseConfig db_conf;
    ArrayList<String> list_clients = new ArrayList<>();
    EditText edt_nom_modif;
    EditText edt_prenom_modif;
    EditText edt_adresse_modif;
    EditText edt_username_modif;
    EditText edt_password_modif;
    int current_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_compte);

        sp_clients = findViewById(R.id.sp_clients);
        edt_nom_modif = findViewById(R.id.edt_nom_modif);
        edt_prenom_modif = findViewById(R.id.edt_prenom_modif);
        edt_adresse_modif = findViewById(R.id.edt_adresse_modif);
        edt_username_modif = findViewById(R.id.edt_username_modif);
        edt_password_modif = findViewById(R.id.edt_password_modif);

        db_conf = new DatabaseConfig(this);

        //extract data
        Cursor res_ = db_conf.getListAccounts_();

        if(res_.getCount()>0){

            while(res_.moveToNext()){
                // colonne 1 : nom_client | colonne 2: prenom_client
                list_clients.add(String.valueOf(res_.getInt(0)));
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, list_clients);
            sp_clients.setAdapter(arrayAdapter);

            //Quand tu cliques sur un item du dropdown
            sp_clients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(ModifierCompteActivity.this, list_clients.get(position), Toast.LENGTH_LONG).show();
                    Cursor res_2 = db_conf.getInfosAccount(Integer.parseInt(list_clients.get(position)));
                    current_id = Integer.parseInt(list_clients.get(position));

                    while(res_2.moveToNext()){
                        // infos client
                        edt_nom_modif.setText(res_2.getString(1));
                        edt_prenom_modif.setText(res_2.getString(2));
                        edt_adresse_modif.setText(res_2.getString(3));
                        edt_username_modif.setText(res_2.getString(4));
                        edt_password_modif.setText(res_2.getString(5));
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

        }else {
            Toast.makeText(this, "Aucun enregistrement trouvé!", Toast.LENGTH_LONG).show();
        }

    }

    public void btn_modifier_client(View view){

        db_conf.updateConfig(current_id, edt_nom_modif.getText().toString(), edt_prenom_modif.getText().toString(), edt_adresse_modif.getText().toString(), edt_username_modif.getText().toString(), edt_password_modif.getText().toString());
        Toast.makeText(this, "Modifié!", Toast.LENGTH_LONG).show();
        finish();

    }

    public void btn_retour(View view){
        //finir tous les processus de l'activité actuelle
        finish();
    }
}