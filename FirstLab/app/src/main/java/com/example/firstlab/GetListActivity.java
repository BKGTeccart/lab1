package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class GetListActivity extends AppCompatActivity {

    DatabaseConfig db_conf;
    ListView listview_accounts;
    ArrayList<String> list_clients = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_list);

        listview_accounts = findViewById(R.id.listview_accounts);

        db_conf = new DatabaseConfig(this);

        //extract data
        Cursor res_ = db_conf.getListAccounts();

        if(res_.getCount()>0){

            while(res_.moveToNext()){
                // colonne 1 : nom_client | colonne 2: prenom_client
                list_clients.add(res_.getString(1) + " " + res_.getString(2));
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, list_clients);
            listview_accounts.setAdapter(arrayAdapter);

        }else {
            Toast.makeText(this, "Aucun enregistrement trouvé!", Toast.LENGTH_LONG).show();
        }


    }

    public void btn_retour(View view){
        //finir tous les processus de l'activité actuelle
        finish();
    }

}