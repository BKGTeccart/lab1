package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CreerCompteActivity extends AppCompatActivity {

    DatabaseConfig db_conf;
    EditText edt_nom;
    EditText edt_prenom;
    EditText edt_adresse;
    EditText edt_username;
    EditText edt_password;
    EditText edt_solde;
    EditText edt_credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_compte);

        db_conf = new DatabaseConfig(this);

        edt_nom = findViewById(R.id.edtnom);
        edt_prenom = findViewById(R.id.edt_prenom);
        edt_adresse = findViewById(R.id.edt_adresse);
        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);
        edt_solde = findViewById(R.id.edt_solde);
        edt_credit = findViewById(R.id.edt_credit);


    }


    public void btn_retour(View view){
        //finir tous les processus de l'activité actuelle
        finish();
    }

    public void btn_inscrire(View view){
        //verification des inputs

        if(edt_nom.getText().toString().equals("") || edt_prenom.getText().toString().equals("") || edt_adresse.getText().toString().equals("") || edt_username.getText().toString().equals("") || edt_password.getText().toString().equals("") || edt_solde.getText().toString().equals("") || edt_credit.getText().toString().equals("")){
            Toast.makeText(this, "Entrées invalides", Toast.LENGTH_LONG).show();
            edt_nom.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_prenom.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_adresse.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_username.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_password.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_solde.setHintTextColor(getResources().getColor(R.color.purple_200));
            edt_credit.setHintTextColor(getResources().getColor(R.color.purple_200));
        } else {
            edt_nom.setHintTextColor(getResources().getColor(R.color.black));
            edt_prenom.setHintTextColor(getResources().getColor(R.color.black));
            edt_adresse.setHintTextColor(getResources().getColor(R.color.black));
            edt_username.setHintTextColor(getResources().getColor(R.color.black));
            edt_password.setHintTextColor(getResources().getColor(R.color.black));
            edt_solde.setHintTextColor(getResources().getColor(R.color.black));
            edt_credit.setHintTextColor(getResources().getColor(R.color.black));


            clsClient compte_cl = new clsClient(edt_nom.getText().toString(), edt_prenom.getText().toString(), edt_adresse.getText().toString(), edt_username.getText().toString(), edt_password.getText().toString(), Double.parseDouble(edt_solde.getText().toString()), Double.parseDouble(edt_credit.getText().toString()));

            //traitement de la base de donnees
            boolean res = db_conf.insert_client(compte_cl.getNom(), compte_cl.getPrenom(), compte_cl.getAdresse(), compte_cl.getUsername(), compte_cl.getPassword(), compte_cl.getSolde(), compte_cl.getCredit());

            if(res){
                Toast.makeText(this, "Inscrit!", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(this, "Erreur d'insertion", Toast.LENGTH_LONG).show();
            }
        }

    }



}