package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.firstlab.clsGlobal.compte_client;


public class MainActivity extends AppCompatActivity {

    EditText txt_username;
    EditText txt_password;
    Button btn_log;
    String tt = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_username = findViewById(R.id.edt_username);
        txt_password = findViewById(R.id.edt_password);
        btn_log = findViewById(R.id.btn_login);

    }

    //Onclick 
    public void btn_login_(View login){

        //recuperer username & password
        compte_client = new clsClient();
        compte_client.setUsername(txt_username.getText().toString());
        compte_client.setPassword(txt_password.getText().toString());

        //verification
        if(txt_username.getText().toString().equals("") || txt_password.getText().toString().equals("")){
            Toast.makeText(this, "Entrées invalides", Toast.LENGTH_LONG).show();
            txt_username.setHintTextColor(getResources().getColor(R.color.purple_200));
            txt_password.setHintTextColor(getResources().getColor(R.color.purple_200));
        } else {
            txt_username.setTextColor(getResources().getColor(R.color.black));
            txt_password.setTextColor(getResources().getColor(R.color.black));

            //traitement de la base de donnees


        }


        //ouvrir dashboard


        //pour les tests
        ///Toast.makeText(this, compte_client.getUsername(), Toast.LENGTH_LONG).show();

    }


}