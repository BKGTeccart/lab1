package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class GetInfoActivity extends AppCompatActivity {

    //variables
    DatabaseConfig db_conf;
    ArrayList<String> list_clients = new ArrayList<>();
    TextView get_nom;
    TextView get_prenom;
    TextView get_adresse;
    TextView get_solde;
    TextView get_credit;
    Spinner sp_get_info;
    int current_id = -1;


    //fonction main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_info);
        db_conf = new DatabaseConfig(this);

        sp_get_info = findViewById(R.id.sp_clientsinfo);
        get_nom = findViewById(R.id.get_nom_view);
        get_prenom = findViewById(R.id.get_prenom_view);
        get_adresse = findViewById(R.id.get_adresse_view);
        get_solde = findViewById(R.id.get_solde_view);
        get_credit = findViewById(R.id.get_credit_view);


        Cursor res_ = db_conf.getListAccounts_();
        if(res_.getCount()>0){

            while(res_.moveToNext()){
                // colonne 1 : nom_client | colonne 2: prenom_client
                list_clients.add(String.valueOf(res_.getInt(0)));
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, list_clients);
            sp_get_info.setAdapter(arrayAdapter);

            //Quand tu cliques sur un item du dropdown
            sp_get_info.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(ModifierCompteActivity.this, list_clients.get(position), Toast.LENGTH_LONG).show();
                    Cursor res_2 = db_conf.getInfosAccount(Integer.parseInt(list_clients.get(position)));
                    current_id = Integer.parseInt(list_clients.get(position));

                    while(res_2.moveToNext()){
                        // infos client
                        get_nom.setText(res_2.getString(1));
                        get_prenom.setText(res_2.getString(2));
                        get_adresse.setText(res_2.getString(3));
                        // parse number to string : String.valueOf(number)
                        get_solde.setText("$" + String.valueOf(res_2.getDouble(6)));
                        get_credit.setText("$"+String.valueOf(res_2.getDouble(7)));

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

        }else {
            Toast.makeText(this, "Aucun enregistrement trouvé!", Toast.LENGTH_LONG).show();
        }



    }

    //fonctions | methodes | boutons
    public void btn_retour(View view){
        //finir tous les processus de l'activité actuelle
        finish();
    }
}