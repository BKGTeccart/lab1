package com.example.firstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuPrincipalActivity extends AppCompatActivity {

    private DatabaseConfig db_conf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        //Executer la requete de creation de la base de donnees
        db_conf = new DatabaseConfig(this);

    }

    public void btn_new_Acc(View v){

        //navigation entre les activités
        Intent intent = new Intent(this, CreerCompteActivity.class);
        startActivity(intent);

    }

    public void btn_mod_credit(View v){
        Intent intent = new Intent(this, ModifierCreditActivity.class);
        startActivity(intent);
    }

    public void btn_Mod_Sold(View v){
        Intent intent = new Intent(this,ModifierSoldeActivity.class);
        startActivity(intent);

    }

    public void btn_mod_acc(View v){

        Intent intent = new Intent(this,ModifierCompteActivity.class);
        startActivity(intent);

    }

    public void btn_get_acc_info(View v){
        Intent intent = new Intent(this,GetInfoActivity.class);
        startActivity(intent);

    }

    public void btn_list_cli_delinquant(View v){
        Intent intent = new Intent(this,GetListActivity.class);
        startActivity(intent);
    }

    public void btn_sup_acc(View v){

        Intent intent = new Intent(this,EffacerCompteActivity.class);
        startActivity(intent);

    }

}